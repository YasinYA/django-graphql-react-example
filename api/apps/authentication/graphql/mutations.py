import graphene
import graphql_jwt

from authentication.models import CustomUser
from authentication.graphql import CustomUserType, CustomUserInputType


class CreateUser(graphene.Mutation):
    """CustomUser Mutation.
    This class creates the user in the database.
    """

    user = graphene.Field(CustomUserType)

    class Arguments:
        user_data = CustomUserInputType()

    def mutate(self, info, user_data):

        if not user_data.io_number:
            raise ValueError('The Immigration Officer Number is Required')

        user = CustomUser(
            name=user_data.name,
            io_number=user_data.io_number
        )

        user.set_password(user_data.password)
        user.save()

        return CreateUser(user=user)


class AuthMutations(graphene.AbstractType):
    """Abstract Mutation Class
    It unites all the app level mutations
    """

    create_user = CreateUser.Field()
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
