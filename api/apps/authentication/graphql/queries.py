import graphene
from graphql_jwt.decorators import login_required
from authentication.graphql import CustomUserType


class AuthQueries(graphene.AbstractType):
    """Abstract query class for authentication app level queries."""

    user_info = graphene.Field(
        CustomUserType,
        # token = graphene.String(required=True)
    )

    @login_required
    def resolve_user_info(self, info, **kwargs):
        return info.context.user
