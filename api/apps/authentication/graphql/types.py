import graphene
from graphene_django import types
from authentication.models import CustomUser


class CustomUserType(types.DjangoObjectType):
    """CustomUser Graphql type """

    class Meta:
        model = CustomUser


class CustomUserInputType(graphene.InputObjectType):
    """CustomUser Graphql input type.
    Used for mutations arguments
    """

    name = graphene.String(required=True)
    io_number = graphene.String(required=True)
    password = graphene.String(required=True)
