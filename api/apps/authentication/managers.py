from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    """Custom user manager
    IO_number field is used as username and unique identifier
    for authentication
    """

    def create_user(self, io_number, password, **extra_fields):
        """Create user with a given Immigration Officer Number
        and password
        """

        if not io_number:
            raise ValueError('Immigration Officer Number is required')

        user = self.model(io_number=io_number, **extra_fields)
        user.set_password(password)
        user.save()

        return user


    def create_superuser(self, io_number, password, **extra_fields):
        """Create superuser(admin) with a given Immigration Officer Number
        and password
        """

        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError("Superuser should have is_stuff=True")
        if extra_fields.get('is_superuser') is not True:
            raise ValueError("Superuser should have is_superuser=True")

        return self.create_user(io_number, password, **extra_fields)
