from django.db import models
from django.contrib.auth.models import AbstractUser

from .managers import CustomUserManager


class CustomUser(AbstractUser):
    name = models.CharField(max_length=150)
    io_number = models.CharField(max_length=15, unique=True)
    username = None

    USERNAME_FIELD = 'io_number'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def str(self):
        return self.name
