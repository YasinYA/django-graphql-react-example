import graphene

from authentication.graphql import AuthQueries, AuthMutations


class Query(AuthQueries, graphene.ObjectType):
    """Project level Query class
    this class unites all the app level query classes by inheriting from them
    """

    pass


class Mutation(AuthMutations, graphene.ObjectType):
    """Project level Mutation Class
    this class unites all the app level mutation classes by inheriting from them
    """

    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
