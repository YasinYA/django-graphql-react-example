import React from 'react'
import { ApolloProvider } from '@apollo/client'

import { client } from './apolloClient/'
import { LoginForm } from './components/'
import './App.css'

function App() {
    return (
        <ApolloProvider client={client}>
	        <h1>REACT APP</h1>
            <LoginForm />
	    </ApolloProvider>
    )
}

export default App
