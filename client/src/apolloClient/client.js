import { ApolloClient, InMemoryCache, HttpLink } from '@apollo/client'

const token = window.localStorage.getItem('token')
const client = new ApolloClient({
    link: new HttpLink({
          uri: 'http://localhost:8000/api/',
          headers: {
            Authorization: `JWT ${token}`
          }
        }),
    cache: new InMemoryCache()
})

export {client}
