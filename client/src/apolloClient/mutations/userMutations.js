import {gql} from '@apollo/client'

const CREATE_USER = gql`
    mutation($name: String! $ioNumber: String! $password: String!) {
        createUser(name: $name ioNumber: $ioNumber password: $password ) {
            name
            ioNumber
        }
    }
`

const TOKEN_AUTH = gql`
    mutation($ioNumber: String! $password: String!) {
        tokenAuth(ioNumber: $ioNumber password: $password) {
            token
        }
    }
`

const VERIFY_TOKEN = gql`
    mutation($token: String!) {
        verifyToken(token: $token) {
            payload
        }
    }
`

export { CREATE_USER, TOKEN_AUTH, VERIFY_TOKEN }
