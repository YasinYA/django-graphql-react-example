import {gql} from '@apollo/client'

const GET_USER_INFO = gql`
    {
        userInfo {
            id
            name
            ioNumber
        }
    }
`

export { GET_USER_INFO }
