import React, { useState } from 'react';
import { useMutation } from '@apollo/client'

import { UserProfile } from './UserProfile'
import { TOKEN_AUTH } from '../apolloClient/'

const LoginForm = (props) => {
    const [ioNumber, setIoNumber] = useState('')
    const [password, setPassword] = useState('')
    const [success, setSuccess] = useState(false)

    const [tokenAuth, { loading, error }] = useMutation(TOKEN_AUTH)
  return (
    <div style={{width: '400px', border:'1px', borderColor:'#000', borderStyle: 'solid', padding: "20px"}}>
        <h1>Login Form</h1>
      <form
        onSubmit={e => {
          e.preventDefault();
          tokenAuth({
              variables: { ioNumber: ioNumber, password: password },
          })
          .then(result => {
              setSuccess(true)
              window.localStorage.setItem('token', result.data.tokenAuth.token)
              setIoNumber('')
              setPassword('')
          })
        }}
      >
        <input
            value={ioNumber}
            onChange={e => (setIoNumber(e.target.value))}
          placeholder="Io Number"
        />
        <input
            value={password}
            onChange={e => (setPassword(e.target.value))}
          placeholder="Password"
        />
        <button type="submit">Login</button>
      </form>
      {loading && <p>Loading</p>}
      {error && <p>Eroor :)</p>}
      {success && (
          <div>
              <p>Successfully Logged in !!!</p>
              <UserProfile />
          </div>
       )}
    </div>
  )
}

export {LoginForm}
