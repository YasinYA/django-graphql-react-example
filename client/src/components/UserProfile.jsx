import React from 'react';
import { useQuery } from '@apollo/client'

import { GET_USER_INFO } from '../apolloClient/'

const UserProfile = (props) => {
    const { loading, error, data } = useQuery(
        GET_USER_INFO
    )
    console.log(data)

  return (
    <div style={{width: '200px', border:'1px', borderColor:'#000', borderStyle: 'solid', padding: "20px", marginTop: "20px"}}>
        {loading && <p>Loading</p>}
        {error && <p>Eroor :)</p>}
        {
            data ? (
               <div>
                   <p>Name: {data.userInfo.name}</p>
                   <p>Immigration Office Number: {data.userInfo.ioNumber}</p>
               </div>
            ) : <p>Loading</p>
        }
    </div>
  )
}

export {UserProfile};
